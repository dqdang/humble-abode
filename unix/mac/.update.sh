#!/bin/bash

brew update && brew upgrade && brew cleanup && brew doctor

(rm ~/Library/Caches/Homebrew/*.gz) 2> /dev/null

pip install --upgrade pip

pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U

rm -rf ~/Library/Caches/pip/*

cd Downloads

bash .update_repos.sh

source /Users/Administrator/Downloads/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

echo "update -> build -> run"
