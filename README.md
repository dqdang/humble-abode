# humble-abode
Repository to store environment scripts

## Windows
1) Put batch files from ``win`` in ``git`` repository.
2) Run by double clicking any batch file in ``git`` directory.

## Mac
1) Put ``update.sh`` in ``$HOME`` directory.
2) Put ``.update_repos.sh`` in ``Downloads``.
3) Run using ``bash .update.sh`` in ``$HOME`` directory.

## Linux
1) Put ``.update.sh`` in ``git`` directory.
2) Run using ``bash .update.sh`` in ``$HOME`` directory.
